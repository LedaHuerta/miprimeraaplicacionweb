﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MiPrimeraAplicacionWeb.Models;

namespace MiPrimeraAplicacionWeb.Models
{
    public class ClienteCLS
    {
        [Display(Name = "Id Cliente")]
        public int iidcliente { get; set; }

        [Display(Name = "Nombre")]
        public string nombre { get; set; }

        [Display(Name = "Apellido Paterno")]
        public string apPaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        public string apMaterno { get; set; }

        public string email { get; set; }

        public string direccion { get; set; }

        public int iidsexo { get; set; }

        [Display(Name = "Teléfono Fijo")]
        public string telefonoFijo { get; set; }

        public string telefonoCelular { get; set; }

        public int bHabilitado { get; set; }

        
    }
}